
package com.veselskyi;
/**
 * @Author V.Veselskyi
 *
 * @Maven Project
 *
 * @version 0.1
 */
/**
 * Importing java typing library
 */
import java.util.Scanner;
public class Application {
    /**
     *Create new ENTER class for start the programm
     */
    public static void main(String[] args) {
        /**
         * Inject typing library and input some number for count
         */
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        /**
         * N - height number for set how long will be array from numbers Fibonacci
         * n0 - 1st number from count starts
         * n1 - 2nd number from count starts
         * n2 - Previously n0+n1 sum
         */
        long N = in.nextInt();
        long n0 = 1;
        long n1 = 1;
        long n2;
        /**
         * resultate output
         */
        System.out.print(n0 + " " + n1 + " ");
        /**
         * If set ZERO, program output that it`s impossible to output result
         */
        if (N <= 0) {
            System.out.println("Impossible to output result, please enter natural number!");
        }
        /**
         *If user set natural number, program start cycle
         */
        else {
            for (long i = 3; i <= N; i++) {
                n2 = n0 + n1;
                System.out.print(n2 + " ");
                n0 = n1;
                n1 = n2;
            }
        }
        System.out.println();
        in.close();
    }
}